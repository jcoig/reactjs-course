
import { SUN, CLOUD, RAIN, SNOW, THUNDER, DRIZZLE } from '../constants/Weathers';

const getWeatherState = (typeOfWeather) => {

    const { id } = typeOfWeather;

    if ( id < 300)      return THUNDER;
    else if (id < 400)  return DRIZZLE;
    else if (id < 600)  return RAIN;
    else if (id < 700)  return SNOW;
    else if (id === 800)return SUN;
    else                return CLOUD;
}


const transformWeather = (rawWeather) => {
    const { humidity, temp } = rawWeather.main;
    const { speed } = rawWeather.wind;
    
    const typeOfWeather = rawWeather.weather[0];

    const data = { 
        temperature: temp, 
        weatherState: getWeatherState(typeOfWeather),
        humidity, // Object Literal Property Value Shorthand
        wind: speed
    }
    return data;
} 

export default transformWeather;
