import React, { Component } from 'react';
import './App.css';
import LocationList from './components/LocationList';
import ForecastExtended from './components/ForecastExtended'
import { Grid, Col, Row } from 'react-flexbox-grid';

import Paper from "@material-ui/core/Paper";
import AppBar from "@material-ui/core/AppBar";
import Typography from "@material-ui/core/Typography";
import Toolbar from "@material-ui/core/Toolbar";

const cities = [
  "Madrid,es",
  "Santander,es",
  "Washington,us",
  "Mexico,mx",
  "Lima,pe"
];

class App extends Component {
  
  city = "Muyayo";

  handleSelectionLocation = city => {
    this.city = city
    console.log(this.city)
  }

  render() {
    return (
      <Grid>
        <Row>
          <AppBar position="sticky">
            <Toolbar>
              <Typography variant="title" color="inherit">
                WeatherApp
              </Typography>
            </Toolbar>
          </AppBar>
        </Row>
        <Row>
          <Col xs={12} md={6}>
            <LocationList 
              cities = { cities }
              onSelectedLocation={this.handleSelectionLocation}>
            </LocationList>
          </Col>

          <Col xs={12} md={6}>
            <Paper zDepth={4} >
              <div className = "details">
                <ForecastExtended city = {this.city} ></ForecastExtended>
              </div>
            </Paper>
          </Col>
        </Row>
      </Grid>
    );
  }
}

export default App
