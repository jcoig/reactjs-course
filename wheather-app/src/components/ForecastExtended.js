import React, { Component } from 'react'

class ForecastExtended extends Component {
    city = "";
    render() {
        this.city = this.props.city;
        return (
            <h1>Pronostico de {this.city}</h1>
        )
    }
}

export default ForecastExtended