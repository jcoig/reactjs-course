import React from 'react';
import PropTypes from 'prop-types';

const Location = ( { city } ) => {
    //  { city } Destructuring:  // técnica de ES6 LOLASO, GUAPISIMO
    // OTRO EJEMPLO: const { city, otroparametro } = props;

    return (
        <div className = "location-container"> 
            <h3> {city} </h3> 
        </div>
    )
};

Location.propTypes = {
    city: PropTypes.string.isRequired
}

export default Location;