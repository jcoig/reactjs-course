
import '../../styles.css';

import React from 'react';
import { WiDaySunny, WiRain, WiCloud, WiCloudy, WiSnow, WiWindy, WiThunderstorm, WiShowers } from 'weather-icons-react';
import PropTypes from 'prop-types';

const icons = {
    cloud: (<WiCloud size = "40"/>),
    cloudy: (<WiCloudy size = "40"/>),
    sun: (<WiDaySunny size = "40"/>),
    rain: (<WiRain size = "40"/>),
    snow: (<WiSnow size = "40"/>),
    windy:(<WiWindy size = "40"/>), 
    thunder: <WiThunderstorm size = "40"/>, 
    drizzle: <WiShowers size = "40"/>,
}

const WeatherIcon = (weatherState) => {
    return icons[weatherState ? weatherState : 'sun'];
}

const WeatherTemperature = ({ temperature, weatherState }) => (
    <div className = "weather-temperature-container">
        { WeatherIcon(weatherState) }
        { `${temperature}°` }
    </div>
);

// Proptypes sirve como apoyo para hacer un control del tipado en Javascript, intenta suplir el NO-TIPADO haciendo uso de la libreria prop-types, para hacerlo
// basta con añadir esta sección asociada a la clase --> Se checkea en tiempo de ejecución (NO COMPILACION)

WeatherTemperature.propTypes = {
    temperature: PropTypes.number.isRequired,
    weatherState: PropTypes.string.isRequired
}

export default WeatherTemperature;