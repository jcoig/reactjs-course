
import '../../styles.css';

import React from 'react';
import WeatherExtraInfo from './WeatherExtraInfo';
import WeatherTemperature from './WeatherTemperature';
import PropTypes from 'prop-types';

// Destructuring más complejo, básicamente estamos diciendo que dentro del objeto data, se extrae todos esos parámetros, definiendolos como entrada de la función
const WeatherData = ( { data: { temperature, weatherState, humidity, wind } }) => {
    
    return (<div className = 'weather-data-container'> 
        <WeatherTemperature 
            temperature = { temperature } 
            weatherState = { weatherState }></WeatherTemperature>
        <WeatherExtraInfo humidity = { humidity } wind = { wind } ></WeatherExtraInfo>
    </div>);
};

WeatherData.propTypes = {
    data: PropTypes.shape( {
        temperature: PropTypes.number.isRequired,
        weatherState: PropTypes.string.isRequired,
        humidity: PropTypes.number.isRequired,
        wind: PropTypes.number.isRequired
    })
};

export default WeatherData;