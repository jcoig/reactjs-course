
import '../../styles.css';

import React from 'react';
import { WiHumidity, WiWindy } from 'weather-icons-react';
import PropTypes from 'prop-types';

const WeatherExtraInfo = ({ humidity, wind }) => (
    <div className = "weather-extra-info-container">  
        <span> <WiHumidity/> { `${ humidity }% - ` } </span>
        <span> <WiWindy/> { `${ wind }m/s` } </span>
    </div>
);

WeatherExtraInfo.propTypes = {
    humidity: PropTypes.number.isRequired,
    wind: PropTypes.number.isRequired,
}

export default WeatherExtraInfo;
