import React, { Component } from 'react';
import Location from './Location';
import WeatherData from './WeatherData';
import '../styles.css';
import transformWeather from '../../services/transformWeather';

import CircularProgress from '@material-ui/core/CircularProgress';

import PropTypes from 'prop-types';
import getUrlWeatherByLocation from '../../services/getUrlWeatherByLocation';

// Componente de clase vs Componente funcional. --> Componente de clase nos proporciona la capacidad de meternos con los ciclos de vida del componente de React.
class WeatherLocation extends Component {

    constructor( props ) {
        super(props);

        const { city } = props;
        this.state = {
            city: city,
            data: null
        }
        console.log('LIFECYCLE: Constructor');
    }

    // LifeCycle
    componentDidMount() {
        console.log('LIFECYCLE: componentDidMount');
        this.obtainWeatherFromApi();
    }
    // LifeCycle
    componentDidUpdate() {console.log('LIFECYCLE: componentDidUpdate')}

    // LifeCyrcle --> ¿deprecated?
    componentWillMount(){console.log('LIFECYCLE: componentWillMount')}

    // LifeCyrcle --> ¿deprecated?
    componentWillUpdate(){console.log('LIFECYCLE: componentWillUpdate')}
    
    // CONSTRUCTOR --> (WILLMOUNT > RENDER > DIDMOUNT > {ACTION} > WILLUPDATE > RENDER > DIDUPDATE )

    render() {
        console.log('LIFECYCLE: render')

        const { onWeatherLocationClick } = this.props;
        return (
            <div className = "weather-location-container" onClick = {onWeatherLocationClick} >
                <Location city={ this.state.city }></Location>
                { this.data 
                    ? (<WeatherData data = { this.state.data }></WeatherData>) 
                    : <CircularProgress/>}
            </div>
        );  
    } 

    obtainWeatherFromApi() {
        setTimeout(()=> {
            fetch(getUrlWeatherByLocation(this.state.city)).then((raw) => raw.json()).then((data) => { 
                this.data = data;
                const newWeather = transformWeather(data);
                this.setState({ data: newWeather});
            });

        }, 500);
    }

    handleUpdateclick = () => {
        this.obtainWeatherFromApi();
    };

}


WeatherLocation.propTypes = {
    city: PropTypes.string.isRequired,
    onWeatherLocationClick: PropTypes.func
}

export default WeatherLocation;